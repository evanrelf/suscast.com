# suscast.com

Website for the Suscast podcast (https://suscast.com)

## Preflight checklist

1. Download episode from [YouTube](https://www.youtube.com/channel/UCCmGXstblKgOsacgDJl02aQ)
1. Add title, description, chapters, and artwork with [Forecast](https://overcast.fm/forecast) (using [correct capitalization](https://capitalizemytitle.com))
1. Upload MP3 to Backblaze B2
1. Update YouTube `iframe` in `public/index.html` to display latest episode
1. Add episode to `generate-feed.js`
1. Push changes to GitLab

## Forecast options

**Podcast title:** `The SuScast`

**Episode title:** `SuScast Episode X: Lorem Ipsum`

**Summary:**

```text
Welcome to the SuScast. We, the SuSquad, are a group of twenty-somethings who
love pizza and making each other laugh. In each episode, we review a different
pizza, from chain places to mom-and-pop pizzerias. Listen to us talk about
meaningless shit on your morning commute and learn about where to find the best
slice of pie.
```
