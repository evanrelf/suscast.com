#!/usr/bin/env bash

log() {
  echo -e "\033[0;34m>> $*\033[0m" >&2
}

log "Downloading YouTube video"
bash "download.sh" "$1"

log "Generating formatted chapters"
bash "format-chapters.sh"

log "Opening Forecast"
open -a Forecast "tmp/download.m4a"

echo "Podcast title: The SuScast"
echo "Episode title: SuScast Episode X: Lorem Ipsum"
echo "Summary: [copied to clipboard]"
echo "Welcome to the SuScast. We, the SuSquad, are a group of twenty-somethings who \
love pizza and making each other laugh. In each episode, we review a different \
pizza, from chain places to mom-and-pop pizzerias. Listen to us talk about \
meaningless shit on your morning commute and learn about where to find the best \
slice of pie." | tee >(pbcopy)

log "TODO: Upload MP3 to Backblaze B2"

log "TODO: Add episode details to 'generate-feed.js'"

log "TODO: Update YouTube 'iframe' in 'public/index.html' to display latest episode"

log "TODO: Push changes to GitLab"
