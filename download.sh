#!/usr/bin/env bash

youtube_video_url=$1

error() {
  echo -e "\033[0;31mERROR: $*\033[0m" >&2
  exit 1
}

command -v youtube-dl >/dev/null || error "youtube-dl is not installed"
command -v ffmpeg >/dev/null || error "ffmpeg is not installed"

if [ -z "$youtube_video_url" ]; then
  error "No YouTube video URL provided"
fi

youtube-dl \
  --extract-audio \
  --no-playlist \
  --output "tmp/download.%(ext)s" \
  --write-description \
  "$youtube_video_url"
