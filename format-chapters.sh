#!/usr/bin/env bash

description_file=${1:-"tmp/download.description"}

command -v grep >/dev/null || error "grep is not installed"
command -v tr >/dev/null || error "tr is not installed"
command -v xargs >/dev/null || error "xargs is not installed"
command -v awk >/dev/null || error "awk is not installed"
command -v sed >/dev/null || error "sed is not installed"
command -v npx >/dev/null || error "npx is not installed"

grep -E "\d+:\d+" < "$description_file" \
  | tr '\n' ';;' \
  | xargs npx title --no-copy \
  | tr ';;' '\n' \
  | awk -F " - " '{printf "%7s  %s\n", $2, $1}' \
  | sed '/^[[:space:]]*$/d'
