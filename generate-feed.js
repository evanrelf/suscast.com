#!/usr/bin/env node

const Podcast = require("podcast");
const fs = require("fs");

const outputFilename = process.argv[2] || "public/feed.xml";
const baseUrl = "https://suscast.com/"
const cdnBaseUrl = "https://b2.suscast.com/file/suscast/";
const defaultDescription =
  "Welcome to the SuScast. We, the SuSquad, are a group of twenty-somethings" +
  " who love pizza and making each other laugh. In each episode, we review a" +
  " different pizza, from chain places to mom-and-pop pizzerias. Listen to us" +
  " talk about meaningless shit on your morning commute and learn about where" +
  " to find the best slice of pie.";

const feed = new Podcast({
  title: "The SuScast",
  description: defaultDescription,
  siteUrl: baseUrl,
  feedUrl: baseUrl + "feed.xml",
  imageUrl: cdnBaseUrl + "artwork.png",
  author: "The SuScast",
  copyright: "Copyright 2018 The SuScast",
  categories: ["Comedy"],
  pubDate: Date(),
  itunesExplicit: true,
  itunesCategory: [{ text: "Comedy" }]
});

function episode(info) {
  // Required: number, title, date, duration, size
  // Optional: description
  feed.addItem({
    title: `SuScast Episode ${info.number}: ${info.title}`,
    guid: `${info.number}`,
    description: defaultDescription + (info.description ? '\n\n' + info.description : ''),
    date: `${info.date} 12:00:00 PST`,
    length: info.length,
    duration: info.duration,
    enclosure: {
      url: cdnBaseUrl + `episode${info.number}.mp3`,
      size: info.size,
      type: 'audio/mp3'
    },
    itunesEpisode: info.number,
    itunesDuration: info.duration,
    itunesImage: cdnBaseUrl + "artwork.png",
    itunesExplicit: true
  });
}

episode({
  number: 9,
  title: "1st Annual Oscars Episode",
  date: "2/24/19",
  duration: "01:10:20",
  size: 34109724
});

episode({
  number: 8,
  title: "Farewell to 2018",
  date: "1/1/19",
  duration: "01:18:00",
  size: 37779929
});

episode({
  number: 7,
  title: "The Spooktacular",
  date: "10/29/18",
  duration: "01:16:37",
  size: 37121507
});

episode({
  number: 6,
  title: "We Yell About Cereal for Awhile",
  date: "10/8/18",
  duration: "00:59:45",
  size: 29026188
});

episode({
  number: 5,
  title: "NUMBUH FIVE",
  date: "9/3/18",
  duration: "01:22:56",
  size: 40151448,
  description: `<p><a target="_blank" href="https://www.youtube.com/watch?v=GXnQ5TczCE0">Bubblegum Soda Commercial</a></p>`
});

episode({
  number: 4,
  title: "OOOOOH!",
  date: "8/27/18",
  duration: "01:11:58",
  size: 34961074
});

episode({
  number: 3,
  title: "The SuSquad Goes To The Movies",
  date: "8/20/18",
  duration: "00:51:19",
  size: 25054401
});

episode({
  number: 2,
  title: "Peepee & Popo",
  date: "8/12/18",
  duration: "00:57:56",
  size: 28231348,
  description: `<p><a target="_blank" href="https://me.me/i/my-big-tummy-full-of-piss-my-showe-21076388">Aforementioned Shower Pee Meme</a>.</p>`
});

episode({
  number: 1,
  title: "Trust Us, It Gets Better",
  date: "6/13/18",
  duration: "00:50:36",
  size: 24305851,
  description: `<p><a target="_blank" href="https://www.youtube.com/watch?v=sXOb0U1xAD0">Andrew's "Samus vs. Ridley" video from 2008</a>.</p>`
});

fs.writeFileSync(outputFilename, feed.buildXml('  '));
