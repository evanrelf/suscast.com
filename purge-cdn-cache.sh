#!/usr/bin/env bash

CF_ZONE="befc9da014ae4566a3efbd34349d0cf2"
CF_EMAIL="evan@evanrelf.com"

curl \
  --request POST \
  --header "X-Auth-Email: $CF_EMAIL" \
  --header "X-Auth-Key: $CF_AUTH_KEY" \
  --header "Content-Type: application/json" \
  --include \
  --fail \
  --data '{"files":["https://suscast.com/feed.xml", "https://suscast.com/index.html", "https://suscast.com/style.css"]}' \
  "https://api.cloudflare.com/client/v4/zones/$CF_ZONE/purge_cache"
